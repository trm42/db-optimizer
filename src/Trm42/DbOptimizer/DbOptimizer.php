<?php namespace Trm42\DbOptimizer;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class DbOptimizer extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'db:optimize';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Runs MySQL OPTIMIZE TABLE for every table in the database.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		Config::set('database.fetch', \PDO::FETCH_BOTH);
		try {
			if ($tables = DB::select('SHOW TABLES')) {
				foreach ($tables as $row) {
	
					$this->info('OPTIMIZING TABLE ' . $row[0]);
					
					if (!DB::statement('OPTIMIZE TABLE ' . $row[0])) {
						$this->info('Failed for some reason...');
					}
				}
				
			} else {
				$this->error('Couldn\'t get any tables');
			}
		} catch (\PDOException $e) {
			$this->error('Error, PDOException happened: ', $e);
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}