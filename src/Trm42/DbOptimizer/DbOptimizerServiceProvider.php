<?php namespace Trm42\DbOptimizer;

use Illuminate\Support\ServiceProvider;

class DbOptimizerServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['db.optimize'] = $this->app->share(function($app) {
			return new DbOptimizer;
    });
    
    $this->commands('db.optimize');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}